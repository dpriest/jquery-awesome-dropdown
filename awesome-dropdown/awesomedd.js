;(function ( $, window, document, undefined ) {
	
	var pluginName = "AwesomeDD",
		defaults = {
            title:      '',
			dd_elem:    null
		};

	// The actual plugin constructor
	function Plugin( element, options ) {
		this.element = $(element);
		this.options = $.extend( {}, defaults, options );
		this._defaults = defaults;
		this._name = pluginName;

		this.init();
	}

	Plugin.prototype = {

		init: function(){
			// Look for the dropdown element in the href of a link if there
			// wasn't one passed in the options
			if( !this.options.dd_elem ){
				this.options.dd_elem = this.element.attr('href');
			}
			
			// Creating the dd object
			this.options.dd_elem = $(this.options.dd_elem);
			
			this.init_actions();
		},
		
		init_actions: function(){
			var self = this;
			self.element.click(function(e){
				self.pre_show();
				self.show();
				e.preventDefault();
			});
			
			$(window).resize(function(){
				self.center_on( self.element );
			});
			
			// Hiding AwesomeDD when esc is hit
			$(document).keyup(function(e){
				if ( e.keyCode == 27 )
					self.hide();
			});
		},
		
		// Receives a jQuery element and centers our AwesomeDD
		// below it and centered
		center_on: function(target_elem){
			var l_pos = target_elem.position();
			var l_height = target_elem.outerHeight();
			var l_width = target_elem.outerWidth();
			var l_center = (l_width / 2) + l_pos.left;
			var l_bottom = l_pos.top + l_height;
			
			var e_width = $('.awesomedd').outerWidth();
			var e_left = l_center - (e_width / 2);
			var e_top = l_bottom;
			
			var t_width = $('.awesomedd').find('.heading img').outerWidth();
			var t_left = (e_width / 2) - (t_width / 2);
			
			$('.awesomedd').css('left', e_left);
			$('.awesomedd').css('top', e_top);
			
			$('.awesomedd').find('.heading img').css('left', t_left);
		},
		
		pre_show: function(){
			var self = this;

            // Hiding any previous instances
			self.hide();

            // Append the HTML to the body
            $('body').append(
                '<div class="awesomedd">' + 
                    '<div class="heading">' + 
                        '<img src="img/awesomedd.png" />' + 
                        '<span>'+ self.options.title +'</span>' + 
                    '</div>' + 
                    '<div class="main '+ (self.options.content_class ? self.options.content_class : '')+'">' + 
                        self.options.dd_elem.html() + 
                    '</div>' + 
                '</div>'
            );

            self.center_on( self.element );

			if ( this.options.pre_show )
				this.options.pre_show( this.options.dd_elem );
		},
		
		show: function(){
			var self = this;

			$('.awesomedd').fadeIn('fast', function(){
				$('body').unbind().click(function(e){
                    var elem = $(e.target);
                    if( !self.options.dd_elem.has( elem ).length
                        && !elem.hasClass('.' + self.options.dd_elem.attr('class'))
                        && !(elem.parents('div[class^="ui-datepicker"]').length > 0)){
                        self.hide();
                    }
				});

                self.post_show();
			});
		},
		
		post_show: function(){
			if ( this.options.post_show )
				this.options.post_show( $('.awesomedd') );
		},
		
		hide: function(){
			$('body').unbind();
			$('.awesomedd').remove();
		}
	};

	// A really lightweight plugin wrapper around the constructor,
	// preventing against multiple instantiations
	$.fn[pluginName] = function ( options ) {
		return this.each(function() {
			if (!$.data(this, "plugin_" + pluginName)) {
				$.data(this, "plugin_" + pluginName, new Plugin( this, options ));
			}
		});
	};

})( jQuery, window, document );
